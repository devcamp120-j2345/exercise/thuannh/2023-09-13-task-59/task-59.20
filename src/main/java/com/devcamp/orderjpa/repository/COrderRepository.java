package com.devcamp.orderjpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.orderjpa.model.COrder;

public interface COrderRepository extends JpaRepository<COrder, Long>{
    
}
