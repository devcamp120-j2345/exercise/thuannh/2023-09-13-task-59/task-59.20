package com.devcamp.orderjpa.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.orderjpa.model.COrder;
import com.devcamp.orderjpa.repository.COrderRepository;


@RestController
@CrossOrigin
public class COrderController {
    @Autowired
    private COrderRepository orderRepository;

    @GetMapping("/orders")
    public ResponseEntity<ArrayList<COrder>> getAllOrders(){
        try {
            ArrayList<COrder> allOrder = new ArrayList<>();
            orderRepository.findAll().forEach(allOrder::add);
            return new ResponseEntity<ArrayList<COrder>>(allOrder, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}